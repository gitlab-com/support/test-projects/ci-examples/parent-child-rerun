stages:
    - everything

random-fail:
    stage: everything
    script:
        - if [[ $((RANDOM % 2)) -lt 1 ]]; then exit 1; fi

always-works-child1:
    stage: everything
    script:
        - exit 0

always-works-child2:
    stage: everything
    script:
        - exit 0

always-works-child3:
    stage: everything
    script:
        - exit 0